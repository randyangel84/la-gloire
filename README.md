## Getting Started

First, run the development server:

```bash
git clone
npm install 
npm run dev
# or
yarn dev
cd sanity folder
cd install sanity
sanity start 
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

# La-Gloire
This is a beta React JS App about my brand La-Gloire.

Application includes modern design and animations, the ability to add and edit products on the go using a Sanity, all advanced cart, and checkout functionalities, and most importantly, the complete integration with Stripe so that you can cover real payments.

# Main Page
![Capture.PNG](./Capture.PNG)

# Product Page
![Capture1.PNG](./Capture1.PNG)

# Stripe Checkout
![Capture2.PNG](./Capture2.PNG)

# Order Confirmation
![Capture3.PNG](./Capture3.PNG)

# Sanity Edit
![Capture4.PNG](./Capture4.PNG)
